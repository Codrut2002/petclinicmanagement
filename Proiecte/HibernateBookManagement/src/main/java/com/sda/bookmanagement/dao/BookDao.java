package com.sda.bookmanagement.dao;

import com.sda.bookmanagement.HibernateUtils;
import com.sda.bookmanagement.model.Author;
import com.sda.bookmanagement.model.Book;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class BookDao {

    public List<Book> getAllBooks() {
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            List<Book> books = session.createQuery("from Book", Book.class).list();
            return books;
        }catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public void createBook(Book book) {
        Transaction transaction = null;
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the book object
            session.save(book);
            // commit transaction
            transaction.commit();
        }catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }

    public Book findById(long id) {
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            Book book = session.find(Book.class, id);
            session.close();
            return book;
        }catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public Book update(Book book) {
        Transaction transaction = null;
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the book object
            session.update(book);
            // commit transaction
            transaction.commit();
        }catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
        return book;
    }

    public void delete(Book book) {
        Transaction transaction = null;
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            // start s transaction
            transaction = session.beginTransaction();
            // delete the book object
            session.delete(book);
            // commit transaction
            transaction.commit();
        }catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }
}