//package com.sda.bookmanagement.dao;
//
//import com.sda.bookmanagement.HibernateUtils;
//import com.sda.bookmanagement.model.Author;
//import com.sda.bookmanagement.model.Book;
//import com.sda.bookmanagement.model.Review;
//import org.hibernate.Session;
//import org.hibernate.Transaction;
//
//public abstract class ItemDao {
//
//    public void createItemDao(ItemDao itemDao) {
//        Transaction transaction = null;
//        try {
//            Session session = HibernateUtils.getSessionFactory().openSession();
//            // start a transaction
//            transaction = session.beginTransaction();
//            // save the itemDao object
//            session.save(itemDao);
//            // commit transaction
//            transaction.commit();
//        }catch (Exception ex) {
//            if (transaction != null) {
//                transaction.rollback();
//            }
//            ex.printStackTrace();
//        }
//    }
//
//    public void update(ItemDao itemDao) {
//        Transaction transaction = null;
//        try {
//            Session session = HibernateUtils.getSessionFactory().openSession();
//            // start a transaction
//            transaction = session.beginTransaction();
//            // save the itemDao object
//            session.update(itemDao);
//            // commit transaction
//            transaction.commit();
//        }catch (Exception ex) {
//            if (transaction != null) {
//                transaction.rollback();
//            }
//            ex.printStackTrace();
//        }
//    }
//
//    public void delete(ItemDao itemDao) {
//        Transaction transaction = null;
//        try {
//            Session session = HibernateUtils.getSessionFactory().openSession();
//            // start s transaction
//            transaction = session.beginTransaction();
//            // delete the itemDao object
//            session.delete(itemDao);
//            // commit transaction
//            transaction.commit();
//        }catch (Exception ex) {
//            if (transaction != null) {
//                transaction.rollback();
//            }
//            ex.printStackTrace();
//        }
//    }
//
//    public abstract void createItemDao(Author author);
//
//    public abstract void update(Author author);
//}
