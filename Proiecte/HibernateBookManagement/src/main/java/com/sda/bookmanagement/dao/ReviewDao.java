package com.sda.bookmanagement.dao;

import com.sda.bookmanagement.HibernateUtils;
import com.sda.bookmanagement.model.Author;
import com.sda.bookmanagement.model.Review;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class ReviewDao {

    public List<Review> getAllReviews() {
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            List<Review> reviews = session.createQuery("from Review", Review.class).list();
            return reviews;
        }catch (Exception ex){
            ex.printStackTrace();
            return null;
        }
    }

    public void createReview(Review review) {
        Transaction transaction = null;
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the review object
            session.save(review);
            // commit transaction
            transaction.commit();
        }catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }

    public Review findById(long id) {
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            Review review = session.find(Review.class, id);
            return review;
        }catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public Review update(Review review) {
        Transaction transaction = null;
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the review object
            session.update(review);
            // commit transaction
            transaction.commit();
        }catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
        return review;
    }

    public void delete(Review review) {
        Transaction transaction = null;
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            // start s transaction
            transaction = session.beginTransaction();
            // delete the review object
            session.delete(review);
            // commit transaction
            transaction.commit();
        }catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }
}
