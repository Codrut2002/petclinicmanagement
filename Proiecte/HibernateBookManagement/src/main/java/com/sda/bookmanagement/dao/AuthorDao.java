package com.sda.bookmanagement.dao;

import com.sda.bookmanagement.HibernateUtils;
import com.sda.bookmanagement.model.Author;
import com.sda.bookmanagement.model.Book;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class AuthorDao {

    public List<Author> getAllAuthors() {
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            List<Author> authors = session.createQuery("from Author", Author.class).list();
            return authors;
        }catch (Exception ex){
            ex.printStackTrace();
            return null;
        }
    }

    public void createAuthor(Author author) {
        Transaction transaction = null;
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the author object
            session.save(author);
            // commit transaction
            transaction.commit();
        }catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }

    public Author findById(long id) {
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            Author author = session.find(Author.class, id);
            session.close();
            return author;
        }catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public void update(Author author) {
        Transaction transaction = null;
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the author object
            session.update(author);
            // commit transaction
            transaction.commit();
        }catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }

    public void delete(Author author) {
        Transaction transaction = null;
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            // start s transaction
            transaction = session.beginTransaction();
            // delete the author object
            session.delete(author);
            // commit transaction
            transaction.commit();
        }catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }
}
