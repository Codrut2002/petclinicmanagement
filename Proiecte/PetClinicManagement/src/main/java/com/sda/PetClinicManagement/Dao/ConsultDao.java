package com.sda.PetClinicManagement.Dao;

import com.sda.PetClinicManagement.HibernateUtils;
import com.sda.PetClinicManagement.Model.Consult;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class ConsultDao {

    public List<Consult> getAllConsults() {
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            List<Consult> consults = session.createQuery("from Consult", Consult.class).list();
            return consults;
        }catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public void createConsult(Consult consult) {
        Transaction transaction = null;
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the consult object
            session.save(consult);
            // commit transaction
            transaction.commit();
        }catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }

    public Consult findById(long id) {
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            Consult consult = session.find(Consult.class, id);
            session.close();
            return consult;
        }catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public void update(Consult consult) {
        Transaction transaction = null;
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the consult object
            session.update(consult);
            // commit transaction
            transaction.commit();
        }catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }

    public void delete(Consult consult) {
        Transaction transaction = null;
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            // start s transaction
            transaction = session.beginTransaction();
            // delete the consult object
            session.delete(consult);
            // commit transaction
            transaction.commit();
        }catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }
}
