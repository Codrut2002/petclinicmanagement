package com.sda.PetClinicManagement.Model;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table
public class Pet {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    @Column(name = "race")
    private String race;

    @Column(name = "birthday")
    private Date birthday;

    @Column(name = "isVaccinated")
    private boolean isVaccinated;

    @Column(name = "ownerName")
    private String ownerName;

    @OneToMany(mappedBy = "pet")
    private List<Consult> consultList;

    public Pet() {

    }

    public Pet(String race, Date birthday, boolean isVaccinated, String ownerName) {
        this.race = race;
        this.birthday = birthday;
        this.isVaccinated = isVaccinated;
        this.ownerName = ownerName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public boolean isVaccinated() {
        return isVaccinated;
    }

    public void setVaccinated(boolean vaccinated) {
        isVaccinated = vaccinated;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    @Override
    public String toString() {
        return "Pet{" +
                "id=" + id +
                ", race='" + race + '\'' +
                ", birthday=" + birthday +
                ", isVaccinated=" + isVaccinated +
                ", ownerName='" + ownerName + '\'' +
                "}\n";
    }
}
