package com.sda.PetClinicManagement.Dao;

import com.sda.PetClinicManagement.HibernateUtils;
import com.sda.PetClinicManagement.Model.Veterinarian;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class VeterinarianDao {

    public List<Veterinarian> getAllVeterinarians() {
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            List<Veterinarian> veterinarians = session.createQuery("from Veterinarian", Veterinarian.class).list();
            return veterinarians;
        }catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public void createVeterinarian(Veterinarian veterinarian) {
        Transaction transaction = null;
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the veterinarian object
            session.save(veterinarian);
            // commit transaction
            transaction.commit();
        }catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }

    public Veterinarian findById(long id) {
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            Veterinarian veterinarian = session.find(Veterinarian.class, id);
            session.close();
            return veterinarian;
        }catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public void update(Veterinarian veterinarian) {
        Transaction transaction = null;
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the veterinarian object
            session.update(veterinarian);
            // commit transaction
            transaction.commit();
        }catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }

    public void delete(Veterinarian veterinarian) {
        Transaction transaction = null;
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            // start s transaction
            transaction = session.beginTransaction();
            // delete the veterinarian object
            session.delete(veterinarian);
            // commit transaction
            transaction.commit();
        }catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }
}
