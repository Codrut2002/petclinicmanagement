package com.sda.PetClinicManagement.Menu;

import com.sda.PetClinicManagement.HibernateUtils;
import org.hibernate.Session;

import java.text.ParseException;

public class Main {
    public static void main(String[] args) throws ParseException, IllegalArgumentException {

        Session s = HibernateUtils.getSessionFactory().openSession();
        s.close();
        Menu menu = new Menu();
        int option = 1;
        while (option != 0) {
            menu.displayMenu();
            option = menu.chooseOption();
        }
    }
}
