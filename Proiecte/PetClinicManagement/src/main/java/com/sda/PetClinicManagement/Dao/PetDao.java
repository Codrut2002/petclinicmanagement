package com.sda.PetClinicManagement.Dao;

import com.sda.PetClinicManagement.HibernateUtils;
import com.sda.PetClinicManagement.Model.Pet;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class PetDao {

    public List<Pet> getAllPets() {
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            List<Pet> pets = session.createQuery("from Pet", Pet.class).list();
            return pets;
        }catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public void createPet(Pet pet) {
        Transaction transaction = null;
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the pet object
            session.save(pet);
            // commit transaction
            transaction.commit();
        }catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }

    public Pet findById(long id) {
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            Pet pet = session.find(Pet.class, id);
            session.close();
            return pet;
        }catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public void update(Pet pet) {
        Transaction transaction = null;
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the pet object
            session.update(pet);
            // commit transaction
            transaction.commit();
        }catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }

    public void delete(Pet pet) {
        Transaction transaction = null;
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            // start s transaction
            transaction = session.beginTransaction();
            // delete the veterinarian object
            session.delete(pet);
            // commit transaction
            transaction.commit();
        }catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }
}
