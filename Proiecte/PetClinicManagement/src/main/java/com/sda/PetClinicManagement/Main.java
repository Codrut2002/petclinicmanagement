package com.sda.PetClinicManagement;

import com.sda.PetClinicManagement.Dao.ConsultDao;
import com.sda.PetClinicManagement.Dao.PetDao;
import com.sda.PetClinicManagement.Dao.VeterinarianDao;
import com.sda.PetClinicManagement.Model.Consult;
import com.sda.PetClinicManagement.Model.Pet;
import com.sda.PetClinicManagement.Model.Veterinarian;

import java.sql.Date;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        // Create Veterinarian

        VeterinarianDao veterinarianDao = new VeterinarianDao();
//        Veterinarian veterinarian = new Veterinarian("Cecilia", "Chapman", "7292 Dictum Av", "dog veterinarian");
//        veterinarianDao.createVeterinarian(veterinarian);
//        List<Veterinarian> veterinarianList = veterinarianDao.getAllVeterinarians();
//        System.out.println("Lista de veterinari este: " + veterinarianList);

        // Update Veterinarian

//        Veterinarian vetUpdate = veterinarianDao.findById(2L);
//        System.out.println("Veterinarul cautat este: " + vetUpdate);
//        System.out.println();
//        vetUpdate.setFirstName("Iris");
//        vetUpdate.setLastName("Watson");
//        vetUpdate.setAddress("P.O. Box 283 8562 Fusce Rd");
//        vetUpdate.setSpeciality("cat veterinarian");
//        veterinarianDao.update(vetUpdate);

        // Delete Veterinarian

//        Veterinarian vetDelete = veterinarianDao.findById(2L);
//        System.out.println("Veterinarul cautat este: " + vetDelete);
//        System.out.println();
//        veterinarianDao.delete(vetDelete);

        // Create Pet

        PetDao petDao = new PetDao();
//        Date date = Date.valueOf("2005-01-11");
//        Pet pet = new Pet("Dog-Akita", date, true, "Mike Thomas");
//        petDao.createPet(pet);
//        List<Pet> petList = petDao.getAllPets();
//        System.out.println("The list of pets is: " + petList);

        // Create Consult

        ConsultDao consultDao = new ConsultDao();
//        Veterinarian veterinarian = veterinarianDao.findById(1);
//        Pet pet = petDao.findById(1);
//        Date date = Date.valueOf("2021-02-05");
//        Consult consult = new Consult(veterinarian, pet, date, "consultation");
//        consultDao.createConsult(consult);
//        List<Consult> consultList = consultDao.getAllConsults();
//        System.out.println("The list of consults is: " + consultList);


    }
}
