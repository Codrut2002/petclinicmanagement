package com.sda.PetClinicManagement.Model;

import javax.persistence.*;
import java.sql.Date;

import static javax.persistence.GenerationType.IDENTITY;


@Entity
@Table
public class Consult {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "veterinarianId")
    private Veterinarian veterinarian;

    @ManyToOne
    @JoinColumn(name = "petId")
    private Pet pet;

    @Column(name = "date")
    private Date date;

    @Column(name = "description")
    private String description;

    public Consult() {

    }

    public Consult(Veterinarian veterinarian, Pet pet, Date date, String description) {
        this.veterinarian = veterinarian;
        this.pet = pet;
        this.date = date;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Veterinarian getVeterinarian() {
        return veterinarian;
    }

    public void setVeterinarian(Veterinarian veterinarian) {
        this.veterinarian = veterinarian;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Consult{" +
                "id=" + id +
                ", veterinarian=" + veterinarian +
                ", pet=" + pet +
                ", date=" + date +
                ", description='" + description + '\'' +
                "}\n\n";
    }
}
