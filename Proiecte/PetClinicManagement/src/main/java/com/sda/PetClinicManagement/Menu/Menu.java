package com.sda.PetClinicManagement.Menu;

import com.sda.PetClinicManagement.Dao.ConsultDao;
import com.sda.PetClinicManagement.Dao.PetDao;
import com.sda.PetClinicManagement.Dao.VeterinarianDao;
import com.sda.PetClinicManagement.Model.Consult;
import com.sda.PetClinicManagement.Model.Pet;
import com.sda.PetClinicManagement.Model.Veterinarian;

import java.sql.Date;
import java.text.ParseException;
import java.util.List;
import java.util.Scanner;

public class Menu {

    private VeterinarianDao veterinarianDao = new VeterinarianDao();
    private PetDao petDao = new PetDao();
    private ConsultDao consultDao = new ConsultDao();
    private Scanner in = new Scanner(System.in);

    public void displayMenu(){
        System.out.println("0. Exit\n" +
                "1. Create Veterinarian\n" +
                "2. Delete Veterinarian\n" +
                "3. Update Veterinarian\n" +
                "4. Show Veterinarians\n" +
                "5. Create Pet\n" +
                "6. Delete Pet\n" +
                "7. Update Pet\n" +
                "8. Show Pets\n" +
                "9. Create Consult\n" +
                "10. Update Consult\n" +
                "11. Show Consults");
    }

    public int chooseOption() throws ParseException {
        System.out.print("Choose an option: ");
        int option = in.nextInt();
        switch (option){
            case 0:
                System.out.println("Program is closing...");
                return option;
            case 1:
                // Create a Veterinarian
                createVeterinarian();
                break;
            case 2:
                // Delete a Veterinarian
                deleteVeterinarian();
                break;
            case 3:
                // Update a Veterinarian
                updateVeterinarian();
                break;
            case 4:
                // Show all Veterinarians
                showVeterinarians();
                break;
            case 5:
                // Create a Pet
                createPet();
                break;
            case 6:
                // Delete a Pet
                deletePet();
                break;
            case 7:
                // Update a Pet
                updatePet();
                break;
            case 8:
                // Show all Pet
                showPets();
                break;
            case 9:
                // Create Consult
                createConsult();
                break;
            case 10:
                // Update a Consult
                updateConsult();
                break;
            case 11:
                // Show all Consults
                showConsults();
                break;
            default:
                System.out.println("Option is invalid");
                this.chooseOption();
        }
        return 1;
    }

    private void showVeterinarians(){
        System.out.println();
        System.out.println("These are the Veterinarians from database:");
        System.out.println();
        List<Veterinarian> veterinarianList = veterinarianDao.getAllVeterinarians();
        System.out.println(veterinarianList);
        System.out.println();
    }

    public void showPets() {
        System.out.println();
        System.out.println("These are the Pets from database: ");
        System.out.println();
        List<Pet> petList = petDao.getAllPets();
        System.out.println(petList);
        System.out.println();
    }

    public void showConsults() {
        System.out.println();
        System.out.println("These are the Consults from database: ");
        System.out.println();
        List<Consult> consultList = consultDao.getAllConsults();
        System.out.println(consultList);
        System.out.println();
    }

    private void createVeterinarian(){
        System.out.println("You chose to create a Veterinarian");
        System.out.print("First Name: ");
        in.nextLine();
        String firstName = in.nextLine();
        System.out.print("Last Name: ");
        String lastName = in.nextLine();
        System.out.print("Address: ");
        String address = in.nextLine();
        System.out.print("Speciality: ");
        String speciality = in.nextLine();
        Veterinarian veterinarian = new Veterinarian(firstName, lastName, address, speciality);
        veterinarianDao.createVeterinarian(veterinarian);
        System.out.println("The Veterinarian was created successfully");
    }

    private void createPet() throws ParseException {
        System.out.println("You chose to create a Pet");
        System.out.print("Race: ");
        in.nextLine();
        String race = in.nextLine();
        System.out.print("Date: ");
        Date date = Date.valueOf(in.nextLine());
        System.out.print("Is vaccinated: ");
        Boolean isVaccinated = in.nextBoolean();
        System.out.print("Owner name: ");
        in.nextLine();
        String ownerName = in.nextLine();
        Pet pet = new Pet(race, date, isVaccinated, ownerName);
        petDao.createPet(pet);
        System.out.println("The pet was created successfully");
    }

    public void createConsult() {
        System.out.println();
        System.out.println("You chose to create a Consult");
        System.out.println();
        System.out.println("Choose a Veterinarian");
        showVeterinarians();
        System.out.print("Veterinarian id: ");
        int veterinarianId = in.nextInt();
        Veterinarian veterinarian = veterinarianDao.findById(veterinarianId);
        System.out.println("Choose a Pet");
        showPets();
        System.out.print("Pet id: ");
        int petId = in.nextInt();
        Pet pet = petDao.findById(petId);
        System.out.print("Choose a Date: ");
        in.nextLine();
        Date date = Date.valueOf(in.nextLine());
        System.out.print("Choose a description: ");
        String description = in.nextLine();
        Consult consult = new Consult(veterinarian, pet, date, description);
        consultDao.createConsult(consult);
        System.out.println("The consult was created successfully");
    }

    private void deleteVeterinarian() {
        System.out.println("You chose to delete a Veterinarian");
        showVeterinarians();
        System.out.print("Which Veterinarian you want to delete: ");
        int veterinarianId = in.nextInt();
        Veterinarian veterinarian = veterinarianDao.findById(veterinarianId);
        veterinarianDao.delete(veterinarian);
        System.out.println("The Veterinarian was deleted successfully");
    }

    private void deletePet() {
        System.out.println("You chose to delete a pet");
        showPets();
        System.out.print("Which pet you want to delete: ");
        int petId = in.nextInt();
        Pet pet = petDao.findById(petId);
        petDao.delete(pet);
        System.out.println("The pet was deleted successfully");
    }

    private void updateVeterinarian() {
        int option;
        System.out.println("You chose to update a Veterinarian");
        System.out.println("Choose a Veterinarian to update");
        showVeterinarians();
        System.out.print("Veterinarian Id: ");
        int veterinarianId = in.nextInt();
        Veterinarian veterinarian= veterinarianDao.findById(veterinarianId);
        do {
            System.out.println();
            System.out.println("What do you want to update?");
            System.out.println();
            System.out.println("0. Nothing\n" +
                    "1. The First Name\n" +
                    "2. The Last Name\n" +
                    "3. The Address\n" +
                    "4. The Speciality");
            System.out.println();
            System.out.print("Option: ");
            option = in.nextInt();
            switch (option) {
                case 0:
                    System.out.println("Program is closing...");
                    break;
                case 1:
                    // The First name
                    System.out.print("Choose a new First Name: ");
                    in.nextLine();
                    String firstName = in.nextLine();
                    veterinarian.setFirstName(firstName);
                    System.out.println();
                    System.out.println("You have updated the First Name");
                    break;
                case 2:
                    // The Last name
                    System.out.print("Choose a new Last Name: ");
                    in.nextLine();
                    String lastName = in.nextLine();
                    veterinarian.setLastName(lastName);
                    System.out.println();
                    System.out.println("You have updated the Last Name");
                    break;
                case 3:
                    // The Address
                    System.out.print("Choose a new Address: ");
                    in.nextLine();
                    String address = in.nextLine();
                    veterinarian.setAddress(address);
                    System.out.println();
                    System.out.println("You have updated the Address");
                    break;
                case 4:
                    // The Speciality
                    System.out.print("Choose a new Speciality: ");
                    in.nextLine();
                    String speciality = in.nextLine();
                    veterinarian.setSpeciality(speciality);
                    System.out.println();
                    System.out.println("You have updated the Speciality");
                    break;
                default:
                    System.out.println("Option is invalid..");
            }
        }while (option != 0);
        veterinarianDao.update(veterinarian);
    }

    private void updatePet() {
        int option;
        System.out.println("You chose to update a pet");
        System.out.println("Choose a pet to update");
        showPets();
        System.out.print("Pet Id: ");
        int petId = in.nextInt();
        Pet pet = petDao.findById(petId);
        do {
            System.out.println();
            System.out.println("What do you want to update?");
            System.out.println();
            System.out.println("0. Nothing\n" +
                    "1. The Race\n" +
                    "2. The Birthday\n" +
                    "3. Is vaccinated (true / false)\n" +
                    "4. The Owner name");
            System.out.println();
            System.out.print("Option: ");
            option = in.nextInt();
            switch (option) {
                case 0:
                    System.out.println("Program is closing...");
                    break;
                case 1:
                    // The race
                    System.out.print("Choose a new race: ");
                    in.nextLine();
                    String race = in.nextLine();
                    pet.setRace(race);
                    System.out.println();
                    System.out.println("You have updated the race");
                    break;
                case 2:
                    // The birthday
                    System.out.print("Choose a new birthday: ");
                    in.nextLine();
                    Date date = Date.valueOf(in.nextLine());
                    pet.setBirthday(date);
                    System.out.println();
                    System.out.println("You have updated the birthday");
                    break;
                case 3:
                    // Is vaccinated
                    System.out.print("Choose a new status: ");
                    in.nextLine();
                    Boolean status = in.nextBoolean();
                    pet.setVaccinated(status);
                    System.out.println();
                    System.out.println("You have updated the status");
                    break;
                case 4:
                    // The owner name
                    System.out.print("Choose a new owner name: ");
                    in.nextLine();
                    String ownerName = in.nextLine();
                    pet.setOwnerName(ownerName);
                    System.out.println();
                    System.out.println("You have updated the owner name");
                    break;
                default:
                    System.out.println("Option is invalid..");
            }
        }while (option != 0);
        petDao.update(pet);
    }

    private void updateConsult() {
        int option;
        System.out.println("You chose to update a consult");
        System.out.println("Choose a consult to update");
        showConsults();
        System.out.print("Consult Id: ");
        int consultId = in.nextInt();
        Consult consult = consultDao.findById(consultId);
        do {
            System.out.println();
            System.out.println("What do you want to update?");
            System.out.println();
            System.out.println("0. Nothing\n" +
                    "1. The Veterinarian\n" +
                    "2. The Pet\n" +
                    "3. The Date\n" +
                    "4. The Description");
            System.out.println();
            System.out.print("Option: ");
            option = in.nextInt();
            switch (option) {
                case 0:
                    System.out.println("Program is closing...");
                    break;
                case 1:
                    // The Veterinarian
                    System.out.println("Choose a new Veterinarian");
                    System.out.println("These are the Veterinarians from database");
                    showVeterinarians();
                    System.out.print("Veterinarian id: ");
                    int veterinarianId = in.nextInt();
                    Veterinarian veterinarian = veterinarianDao.findById(veterinarianId);
                    consult.setVeterinarian(veterinarian);
                    System.out.println();
                    System.out.println("You have updated the veterinarian");
                    break;
                case 2:
                    // The Pet
                    System.out.println("Choose a new Pet");
                    System.out.println("These are the Pets from database");
                    showPets();
                    System.out.print("Pet id: ");
                    int petId = in.nextInt();
                    Pet pet = petDao.findById(petId);
                    consult.setPet(pet);
                    System.out.println();
                    System.out.println("You have updated the pet");
                    break;
                case 3:
                    // The Date
                    System.out.print("Choose a new date: ");
                    in.nextLine();
                    Date date = Date.valueOf(in.nextLine());
                    consult.setDate(date);
                    System.out.println();
                    System.out.println("You have updated the date");
                    break;
                case 4:
                    // The Description
                    System.out.print("Choose a new description: ");
                    in.nextLine();
                    String description = in.nextLine();
                    consult.setDescription(description);
                    System.out.println();
                    System.out.println("You have updated the description");
                    break;
                default:
                    System.out.println("Option is invalid..");
            }
        }while (option != 0);
        consultDao.update(consult);
    }
}